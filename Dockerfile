FROM debian:buster-20210408-slim
ADD https://curl.haxx.se/ca/cacert.pem /etc/ssl/certs/cacert.pem
ENV CURL_CA_BUNDLE=/etc/ssl/certs/cacert.pem

ARG VERSION
ENV KEYCLOAK_VERSION $VERSION
# https://jdbc.postgresql.org/download.html
ENV JDBC_POSTGRES_VERSION 42.2.24
# https://dev.mysql.com/downloads/connector/j/
ENV JDBC_MYSQL_VERSION 8.0.22
# https://github.com/mariadb-corporation/mariadb-connector-j/releases
ENV JDBC_MARIADB_VERSION 2.7.1
# https://docs.microsoft.com/en-us/sql/connect/jdbc/microsoft-jdbc-driver-for-sql-server-support-matrix?view=sql-server-ver15
ENV JDBC_MSSQL_VERSION 8.4.1.jre11


ENV LAUNCH_JBOSS_IN_BACKGROUND 1
ENV PROXY_ADDRESS_FORWARDING false
ENV JBOSS_HOME /opt/jboss/keycloak
ENV LANG en_US.UTF-8

ARG GIT_REPO
ARG GIT_BRANCH
ARG KEYCLOAK_DIST=https://github.com/keycloak/keycloak/releases/download/$KEYCLOAK_VERSION/keycloak-$KEYCLOAK_VERSION.tar.gz

RUN mkdir -p /usr/share/man/man1 \
  && export DEBIAN_FRONTEND=noninteractive \
  && apt update -qq \
  && apt install -qqy openjdk-11-jre-headless openssl curl \
  && apt-get clean \
  && rm -rf /var/lib/apt

ADD tools /opt/jboss/tools
RUN /opt/jboss/tools/build-keycloak.sh

USER 1000

EXPOSE 8080
EXPOSE 8443

ENTRYPOINT [ "/opt/jboss/tools/docker-entrypoint.sh" ]

CMD ["-b", "0.0.0.0"]
